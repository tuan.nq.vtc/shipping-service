# Shipping service - Travis Nguyen


###################
## Set up the environment
My project is using docker to set up the environment, so make sure that your docker app is running.

Please follow the steps below to run my assignment:

1. Clone repo : `git clone https://gitlab.com/tuan.nq.vtc/shipping-service.git`
2. Run the command: `cd shipping-service/`
3. Check out the master branch : `git checkout master`
4. Start docker: `docker-compose up -d`
5. Check url : http://localhost/ . The page will show the PHP version: 7.4
6. Execute into the container :`docker exec -it nginx-container bash`
7. Go to code folder: `cd var/www/html/`
8. I push the vendor folder to git ( just for saving time :D) , so we can run the test: `vendor/bin/phpunit tests/`


###################
## For the question:
How do we add a fee by product type without changing the  shipping fee code? 


--> 
In the feature, we can create new class: ProductTypeFee. It extends the abstract class Fee. The code likes:  

```injectablephp
class ProductTypeFee extends Fee
{
    const TYPE_PHONE = 'phone';
    const TYPE_DIAMOND = 'diamond';
    public function __construct(string $type)
    {
        parent::__construct([$this->getFeeParameterByType($type)]);
    }
    
    public function getFeeParameterByType($type)
    {
        switch($type) {
            case self::TYPE_PHONE:
                return  $some_config_or_function;
            case self::TYPE_DIAMOND:
                return  $some_config_or_function;
            default: 
             return 0; // free
        }
    }

}

```

and then, the order item can add this fee via function setupFee($otherFee).
The code likes:

```injectablephp
$phoneFee = new  ProductTypeFee(ProductTypeFee::TYPE_PHONE);
$orderItem->setupFee($phoneFee); // the item's price can be re-calculate the item price.  

```
